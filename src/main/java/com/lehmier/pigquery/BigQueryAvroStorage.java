package com.lehmier.pigquery;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import org.apache.pig.piggybank.storage.avro.AvroStorage;
import org.json.simple.parser.ParseException;

/**
 *
 *
 * @author  lehmier
 */
public class BigQueryAvroStorage extends AvroStorage {

    /**
     *
     *
     */
    public static void main(String[] args) {
        System.out.println("HEY!!");
        System.out.println(new BigQueryAvroStorage());
    }

    public BigQueryAvroStorage() {
        super();
    }

    public BigQueryAvroStorage(String[] parts) throws IOException, ParseException {
        super(parts);
        System.out.println("PARTS!!!");
        for (String part : parts) {
            System.out.println(part);
        }
    }

    @Override
    public void setLocation(String location, Job job) throws IOException {
        System.out.print("IN setLocation");
        System.out.println("LOCATION: " + location);

        Configuration conf = job.getConfiguration();
        System.out.println("CONF: " + conf.toString());

        super.setLocation(location, job);
    }

    @Override
    public String relativeToAbsolutePath(String location, Path curDir) throws IOException {
        System.out.println("IN relativeToAbsolutePath");
        System.out.println("LOCATION:  location");
        System.out.println("PATH: " + curDir.toString());
        return super.relativeToAbsolutePath(location, curDir);
    }
}
