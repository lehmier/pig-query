package com.lehmier.pigquery;

import static org.junit.Assert.assertNotNull;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Unit test placeholder
 *
 * @author  lehmier
 */
public class BigQueryAvroStorageTest extends TestCase {

    private BigQueryAvroStorage bqStorage;

    /**
     *
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        bqStorage = new BigQueryAvroStorage();
    }

    /**
     *
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        bqStorage = null;
    }

    /**
     * Sample test
     *
     */
    @Test
    public void testPlaceholder() throws Exception {
        assertNotNull(bqStorage);
    }
}
