# pig-query

## Run the tests

```
mvn clean test
```

## Build the jar

```
mvn clean package
```

## Run the main 

```
java -jar ./target/java -jar ./target/pig-query-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```
