# Scope

This document is an abstract stream-of-consciousness specification intended to
capture ideas and initial thoughts on what this project is intended to be, how
it should work, and anything needed to be hashed out further. 

# Dependencies
- Apache Pig
- Hadoop 2
- Google BigData Interop (https://github.com/GoogleCloudPlatform/bigdata-interop)
- Apache Avro

# Intended Usage

# 